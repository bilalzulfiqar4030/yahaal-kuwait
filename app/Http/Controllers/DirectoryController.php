<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
class DirectoryController extends Controller
{
    public function directory(Request $request)
    {
        $contents = File::get('file:///C:/xampp/htdocs/google-api/mock.txt');
        $array = explode("\n",$contents);
        $array_list=[];
        foreach($array as $key => $ar)
        {
        
            $split = explode(',',$ar);
            if($split[0])
            {
                if($split[3]=="Male")
                {
                
                        $url="http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
                }
                else
                {
                    $url="http://maps.google.com/mapfiles/ms/icons/pink-dot.png";
                }
            $array_list[] =array('first_name'=>$split[1].' '.$split[2],'gender'=>$split[3],'longitude'=>$split[5],'latitude'=>$split[4],'icon'=>$url); 
            }
        }
        
        return json_encode($array_list);
    }
    public function filterByGender($gender)
    {
    
        $contents = File::get('file:///C:/xampp/htdocs/google-api/mock.txt');
        $array = explode("\n",$contents);
        $array_list=[];
        foreach($array as $key => $ar)
        {
        
            $split = explode(',',$ar);
            if($split[0])
            {
                if($split[3]==$gender)
                {
                    if($split[3]=="Male")
                    {
                    
                            $url="http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
                    }
                    else
                    {
                        $url="http://maps.google.com/mapfiles/ms/icons/pink-dot.png";
                    }
                     $array_list[] =array('first_name'=>$split[1].' '.$split[2],'gender'=>$split[3],'longitude'=>$split[5],'latitude'=>$split[4],'icon'=>$url); 
                }
            }
        }
        
        return json_encode($array_list);
    }
}
